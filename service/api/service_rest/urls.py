from django.urls import path
from .views import(
api_list_technicians,
api_list_appointments,
api_delete_tech,
api_appt_detail,
api_appt_cancel,
api_appt_finish
)

urlpatterns = [
    path('technicians/', api_list_technicians, name='api_list_technicians'),
    path('technicians/<int:pk>/', api_delete_tech, name='api_delete_tech'),
    path('appointments/', api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:pk>/', api_appt_detail, name='api_appt_detail'),
    path('appointments/<int:pk>/cancel/', api_appt_cancel, name='api_appt_cancel'),
    path('appointments/<int:pk>/finish/', api_appt_finish, name='api_appt_finish'),
]
