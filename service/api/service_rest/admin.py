from django.contrib import admin
from .models import Technician, Appointment
# Register your models here.
@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = [
        'id',
    ]

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = [
        'id',
    ]
