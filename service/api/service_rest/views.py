from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'employee_id',
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold'
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'reason',
        'vin',
        'customer',
        'technician',
        "is_finished",
        'is_cancelled',
        'is_vip',
        'id',
    ]
    encoders = {
        'technician': TechnicianEncoder()
    }

    def get_extra_data(self, o):
        if isinstance(o.date, str) and isinstance(o.time, str):
            return {
               "date": o.date,
               "time": o.time,
            }
        else:
            return {
                "date": o.date.isoformat(),
                "time": o.time.isoformat(),
            }


@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(['DELETE'])
def api_delete_tech(request, pk):
    if request.method == 'DELETE':
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})


@require_http_methods(['GET', 'POST'])
def api_list_appointments(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        return JsonResponse(
            {'appointments': appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            tech_id = content['technician']
            technician = Technician.objects.get(employee_id=tech_id)
            content['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {'message': 'tech does not exist'}
            )
        if AutomobileVO.objects.filter(vin=content['vin']):
            content['is_vip'] = True
        else:
            content['is_vip'] = False
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(['GET'])
def api_appt_detail(request, pk):
    try:
        appt = Appointment.objects.get(id=pk)
        appt_data = {
            'date': appt.date,
            'time': appt.time,
            'reason': appt.reason,
            'vin': appt.vin,
            'customer': appt.customer,
            'is_finished': appt.is_finished,
            'is_cancelled': appt.is_cancelled,
            'is_vip': appt.is_vip,
            'technician': appt.technician.first_name
        }
        return JsonResponse(
            appt_data,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {'message': 'appt does not exist!'},
            status=404
        )


@require_http_methods(['DELETE'])
def api_appt_delete(request, pk):
    count, _ = Appointment.objects.filter(id=pk).delete()
    return JsonResponse({'deleted': count > 0})


@require_http_methods(['PUT'])
def api_appt_cancel(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        if appointment.is_cancelled:
            return JsonResponse(
                {'message': 'appt already cancelled!'}
            )
        else:
            appointment.is_cancelled = True
            appointment.save()
            return JsonResponse(
                {'message': 'appt cancelled!'}
            )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {'message': 'appt does not exist!'},
            status=404
        )


@require_http_methods(['PUT'])
def api_appt_finish(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        if appointment.is_finished:
            return JsonResponse(
                {'message': 'appt already finished!'}
            )
        else:
            appointment.is_finished = True
            appointment.save()
            return JsonResponse(
                {'message': 'appt finished! Give your mechanics a bonus!'}
            )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {'message': 'appointment does not exist!'},
            status=404
        )
