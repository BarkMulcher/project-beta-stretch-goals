from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date = models.DateField(null=True, blank=True)
    time = models.TimeField(
        null=True,
        blank=True,
        auto_now=False,
        auto_now_add=False,
    )
    reason = models.TextField(max_length=500)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    is_finished = models.BooleanField(default=False)
    is_cancelled = models.BooleanField(default=False)
    is_vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name='technician',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.technician.first_name


