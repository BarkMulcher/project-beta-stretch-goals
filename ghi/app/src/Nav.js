/* eslint-disable */
import { NavLink } from 'react-router-dom';

function Nav() {

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <NavLink className="nav-link active" aria-current="page" to="/">
              <li className="nav-item" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent">Home</li>
            </NavLink>
            <li className='nav-item dropdown'>
              <a className='nav-link dropdown-toggle' role='button' data-bs-toggle='dropdown' aria-expanded='false' href="#">
                Inventory
              </a>
              <ul className='dropdown-menu'>
                <NavLink className="nav-link" to="/mfrList">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Manufacturers</li>
                </NavLink>
                <NavLink className="nav-link" to="/addMfr">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Manufacturer</li>
                </NavLink>
                <NavLink className="nav-link" to="/modelsList">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Models</li>
                </NavLink>
                <NavLink className="nav-link" to="/addModel">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Model</li>
                </NavLink>
                <NavLink className="nav-link" to="/addVeh">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Vehicle</li>
                </NavLink>
                <li>
                  <hr className='dropdown-divider' />
                </li>
                <NavLink className="nav-link" to="/vehList">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Vehicles</li>
                </NavLink>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <a href="#" className='nav-link dropdown-toggle' role='button' data-bs-toggle='dropdown' aria-expanded='false'>
                Service
              </a>
              <ul className='dropdown-menu'>
                <NavLink className="nav-link" to="/apptList">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Appointments</li>
                </NavLink>
                <NavLink className="nav-link" to="/addAppt">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Appointment</li>
                </NavLink>
                <NavLink className="nav-link" to="/techsList">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Technicians</li>
                </NavLink>
                <NavLink className='nav-link' to='/addTech'>
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Technician</li>
                </NavLink>
                <li>
                  <hr className='dropdown-divider' />
                </li>
                <NavLink className="nav-link" to="/svcHistory">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Service History</li>
                </NavLink>
              </ul>
            </li>
            <li className='nav-item dropdown'>
              <a href="#" className='nav-link dropdown-toggle' role='button' data-bs-toggle='dropdown' aria-expanded='false'>
                Sales
              </a>
              <ul className='dropdown-menu'>
                <NavLink className="nav-link" to="/salespeople">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Salespeople</li>
                </NavLink>
                <NavLink className="nav-link" to="/salespeople/new">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Salesperson</li>
                </NavLink>
                <NavLink className="nav-link" to="/customers">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Customers</li>
                </NavLink>
                <NavLink className="nav-link" to="/customers/new">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add Customer</li>
                </NavLink>
                <NavLink className="nav-link" to="/sales/new">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Add a Sale</li>
                </NavLink>
                <NavLink className="nav-link" to="/salespeople/history">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Salesperson History</li>
                </NavLink>
                <li>
                  <hr className='dropdown-divider'/>
                </li>
                <NavLink className="nav-link" to="/sales">
                  <li className="nav-item" data-bs-toggle="collapse" data-bs-target=".navbar-collapse.show">Sales</li>
                </NavLink>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
