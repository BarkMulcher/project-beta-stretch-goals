import React, { useState } from 'react';

function SalespersonForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [empoyeeId, setEmployeeId] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = empoyeeId;

        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            },
        };
        const response = await fetch(salespeopleUrl, fetchConfig);

        if (response.ok) {
            const newSalesperson = await response.json(); // eslint-disable-line no-unused-vars


            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    return (
        <div>
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3" >
                    <input onChange={handleFirstNameChange} value={firstName} placeholder="First Name" required type="text" id="first_name" name="first_name"  className="form-control" />
                    <label htmlFor="first_name" >First Name</label>
                </div>
                <div className="form-floating mb-3" >
                    <input onChange={handleLastNameChange} value={lastName} placeholder="Last Name" required type="text" id="last_name" name="last_name"  className="form-control" />
                    <label htmlFor="last_name" >Last Name</label>
                </div>
                <div className="form-floating mb-3" >
                    <input onChange={handleEmployeeIdChange} value={empoyeeId} placeholder="Employee ID" required type="text" id="employee_id" name="employee_id"  className="form-control" />
                    <label htmlFor="employee_id" >Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )

}

export default SalespersonForm;
