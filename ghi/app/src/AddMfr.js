import React, { useState } from 'react'

function AddMfr() {

    const [name, setName] = useState([]);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const mfrUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(mfrUrl, fetchConfig);
        if (response.ok) {
            const newMfr = await response.json(); // eslint-disable-line no-unused-vars

            setName('');

        }
    }


    return (
<div className="row">
<div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
    <h1>Create A New Manufacturer</h1>
    <form onSubmit={handleSubmit} id="create-hat-form">
        <div className="form-floating mb-3">
        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" name="name"  className="form-control"/>
        <label htmlFor="name">Name</label>
        </div>
        <button className="btn btn-primary">Create</button>
    </form>
    </div>
</div>
</div>
    );
}

export default AddMfr;