import React, { useEffect, useState } from 'react'

function AddTech() {

    const [firstName, setFirstName] = useState([]);
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');


    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmpIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;


// keep working from here 6.6.23 @ 2;11pm
        const hatUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json(); // eslint-disable-line no-unused-vars

            setFirstName('');
            setLastName('');
            setEmployeeId('');

        }
    }

    return (
    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
      <h1>Add A Technician</h1>
      <form onSubmit={handleSubmit} id="create-hat-form">
        <div className="form-floating mb-3">
          <input onChange={handleFirstChange} value={firstName} placeholder="First Name" required type="text" id="first_name" name="first_name"  className="form-control"/>
          <label htmlFor="first_name">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleLastChange} value={lastName} placeholder="Last Name" required type="text" id="last_name" name="last_name"  className="form-control"/>
          <label htmlFor="last_name">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input onChange={handleEmpIdChange} value={employeeId} placeholder="Employee ID" required type="text" id="employeeId" name="employeeId"  className="form-control"/>
          <label htmlFor="emp_id">Employee ID</label>
        </div>
        <button className="btn btn-primary">Create</button>
      </form>
    </div>
    </div>
    </div>
    );
}

export default AddTech;