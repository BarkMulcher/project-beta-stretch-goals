import React from 'react'

function TechsList(props) {

    if (props.techs === undefined) {
        return null
    }

    return (
<>
<main>
<table className='table table-striped'>
<thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Employee ID</th>
    </tr>
</thead>
<tbody>
    {props.techs.map(techs => {
        return (
            <tr key={techs.id}>
                <td>{techs.first_name}</td>
                <td>{techs.last_name}</td>
                <td>{techs.employee_id}</td>
            </tr>
        )
    })}
</tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossOrigin="anonymous">
</script>
</main>
</>
    )
}


export default TechsList