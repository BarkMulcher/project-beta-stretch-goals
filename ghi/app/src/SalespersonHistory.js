import React, { useState, useEffect } from 'react'

// Should include a dropdown that allows someone to choose a salesperson.
// Below should be a table listing ONLY that person's sales

function SalespersonHistory(props) {
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('')

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSelectedSalesperson(value);
    }


    const fetchData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const salespersonResponse = await fetch(salespersonUrl);

        if (salespersonResponse.ok) {
            const salespersonData = await salespersonResponse.json();
            setSalespeople(salespersonData.salespeople);
            }
        }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <main>
            <h1>Salesperson History</h1>
            <div>
                <select onChange={handleSalespersonChange} value={selectedSalesperson} required id="salesperson" name="salesperson" className="form-select">
                    <option value="">Choose a salesperson</option>
                    {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            { salesperson.first_name} { salesperson.last_name}
                        </option>
                    );
                })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {props.sales.map(sale => {
                        if (selectedSalesperson && sale.salesperson.employee_id !== selectedSalesperson){
                            return null;
                        }
                        return (
                            <tr key={sale.id}>
                                <td>{ sale.salesperson.first_name} { sale.salesperson.last_name}</td>
                                <td>{ sale.customer.first_name} { sale.customer.last_name}</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ sale.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </main>
        </>
    )
}

export default SalespersonHistory;
