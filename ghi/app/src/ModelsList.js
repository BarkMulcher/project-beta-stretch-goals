import React from 'react'

function ModelsList(props) {
    if (props.models === undefined) {
        return null
    }


    return (
<>
<main>
<table className='table table-striped'>
<thead>
    <tr>
        <th>Name</th>
        <th>Manufacturer</th>
        <th>Picture</th>
    </tr>
</thead>
<tbody>
    {props.models.map(model => {
        return (
            <tr key={model.href}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td><img  id='car_img' src={model.picture_url} alt="car" /></td>
                {/* className='img-fluid' */}
            </tr>
        )
    })}
</tbody>
</table>
</main>
</>
    )
}


export default ModelsList
