import React, { useEffect, useState } from 'react'

function AddModel() {

    const [name, setName] = useState('');
    const [picUrl, setPicUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([]);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleUrlChange = (event) => {
        const value = event.target.value;
        setPicUrl(value);
    }

    const handleMfrChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.manufacturer_id = manufacturer;
        data.picture_url = picUrl;

        const modelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const newModel = await response.json(); // eslint-disable-line no-unused-vars

            setName('');
            setManufacturer('');
            setPicUrl('');

        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add A Model</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Model Name" required type="text" id="model_name" name="first_name"  className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleUrlChange} value={picUrl} placeholder="Photo URL" required type="text" id="picUrl" name="picUrl"  className="form-control"/>
                <label htmlFor="style_name">Photo URL</label>
              </div>
              <div className="mb-3">
            <select value={manufacturer} onChange={handleMfrChange} required name="mfr" id="mfrs" className="form-select">
            <option value="mfr">Select Manufacturer</option>
            {manufacturers.map(manufacturer => {
                return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                    </option>
                )
            })}
            </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default AddModel;

{/* <div className="form-floating mb-3">
<input onChange={handleMfrChange} value={manufacturer} placeholder="Manufacturer" required type="text" id="mfr" name="manufacturer"  className="form-control"/>
<label htmlFor="fabric">Manufacturer</label>
</div> */}
