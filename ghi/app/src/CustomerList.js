import React from 'react'

function CustomerList(props) {
    if (props.customers === undefined) {
        return null
    }

    return (
        <>
        <main>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {props.customers.map(customer => {
                        return (
                            <tr key={ customer.id }>
                                 <td>{ customer.first_name}</td>
                                 <td>{ customer.last_name}</td>
                                 <td>{ customer.phone_number }</td>
                                 <td>{ customer.address }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </main>
        </>
    )
}

export default CustomerList;
