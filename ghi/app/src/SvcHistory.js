import React, { useState } from 'react';

export default function SvcHistory(props) {
    const [query, setQuery] = useState('');
    const [results, setResults] = useState([]);

    const handleSearch = (event) => {
        setQuery(event.target.value);
        filterAppointments(event.target.value);
    };

    const filterAppointments = (searchQuery) => {
        const filtered = props.appts.filter((appt) =>
            appt.vin.includes(searchQuery)
        );
        setResults(filtered);
    };

    return (
        <>
            <div>
                <h3 className="text-center">Appointment History</h3>
                <form onSubmit={(event) => event.preventDefault()} className='input-group mb-2 mt-5'>
                    <input
                        type="text"
                        value={query}
                        onChange={handleSearch}
                        placeholder="Search by VIN..."
                    />
                    <button type="submit" className="btn btn-outline-primary">
                        Search VIN
                    </button>
                </form>

                <table className="table table-striped mx-1">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Completion Status</th>
                            <th>Cancellation Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {query ? (
                            results.map((appt) => (
                                <tr key={appt.id}>
                                    <td>{appt.vin}</td>
                                    <td>{appt.is_vip ? 'Yes' : 'No'}</td>
                                    <td>{appt.customer}</td>
                                    <td>{appt.date}</td>
                                    <td>{appt.time}</td>
                                    <td>{appt.technician.first_name}</td>
                                    <td>{appt.reason}</td>
                                    <td>{appt.is_finished ? 'finished' : 'created'}</td>
                                    <td>{appt.is_cancelled ? 'cancelled' : 'confirmed'}</td>
                                </tr>
                            ))
                        ) : (
                            props.appts.map((appt) => (
                                <tr key={appt.id}>
                                    <td>{appt.vin}</td>
                                    <td>{appt.is_vip ? 'Yes' : 'No'}</td>
                                    <td>{appt.customer}</td>
                                    <td>{appt.date}</td>
                                    <td>{appt.time}</td>
                                    <td>{appt.technician.first_name}</td>
                                    <td>{appt.reason}</td>
                                    <td>{appt.is_finished ? 'finished' : 'created'}</td>
                                    <td>{appt.is_cancelled ? 'cancelled' : 'confirmed'}</td>
                                </tr>
                            ))
                        )}
                    </tbody>
                </table>
            </div>
        </>
    );
}
