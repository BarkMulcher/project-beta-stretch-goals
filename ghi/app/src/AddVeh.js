import React, { useEffect, useState } from 'react'


function AddVeh() {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')
    const [models, setModels] = useState([])


    const colorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const yearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const vinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const modelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const fetchData = async () => {
        const modelUrl = 'http://localhost:8100/api/models/'

        const response = await fetch(modelUrl)

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
/////////////////////////////

    const submitBtnHandler = async (event) => {
        event.preventDefault()
        // create empty JSON data
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model

        const vehicleUrl = `http://localhost:8100/api/automobiles/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json(); // eslint-disable-line no-unused-vars

            setYear('')
            setVin('')
            setColor('')
            setModel('')
        }
    }




    return (
<>
<main>
<div className="container">
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
    <h1>Add An Automobile To Inventory</h1>
    <form id="create-presentation-form" onSubmit={submitBtnHandler}>
        <div className="form-floating mb-3">
            <input value={color} onChange={colorChange} placeholder="Color..." required type="text" name="color" id="color" className="form-control"></input>
            <label htmlFor="name">Color</label>
        </div>
        <div className="form-floating mb-3">
            <input value={year} onChange={yearChange} name="year" type="number" className="form-control" id="year" placeholder="Year..."></input>
            <label htmlFor="brand">Year</label>
        </div>
        <div className="form-floating mb-3">
            <input value={vin} onChange={vinChange} placeholder="VIN..." type="text" name="vin" id="vin" className="form-control"></input>
            <label htmlFor="color">VIN</label>
        </div>
        <div className="mb-3">
        <select value={model} onChange={modelChange} required name="model" id="model" className="form-select">
        <option value="model">Select Model</option>
        {models.map(model => {
            return (
                <option key={model.id} value={model.id}>
                    {model.name}
                </option>
            )
        })}
        </select>
        </div>
        <button className="btn btn-primary">Create</button>
    </form>
</div>
</div>
</div>
</div>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossOrigin="anonymous">
</script>

</>
    )
}

export default AddVeh