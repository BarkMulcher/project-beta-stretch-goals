# CarCar

Team:

* Person 1 - Which microservice?
MK Sales

* Person 2 - Which microservice?
Luke Service

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

1. API Development
    - Create 3 models and the VO:
        1. Salesperson
            - first_name
            - last_name
            - employee_id
        2. Customer
            - first_name
            - last_name
            - address
            - phone_number
        3. Sale
            - automobile (foreign key - Inventory/models: Automobile)
            - salesperson (foreign key to Salesperson model)
            - customer (foreign key to customer model)
            - price
        4. Automobile VO
            - vin
            - sold
    - Create views that allow get, post, put and delete features
        - Encoders: Salesperson, Customer, AutomobileVO, Sale
        - Views: list salespeople, show salesperson, list customers, show customer, list  sales, show sale
    - Update urls
        - sales_rest/urls.py
        - sales_project/urls.py
    - Automobile Poller
        - update poller.py
        - run tests
2. Front-End Development
    - Add a Salesperson X
        - Form
            - first name
            - last name
            - employee ID
            - create
        - Add to Navbar: Add a Salesperson
    - List all Salespeople X
        - list (table)
            - employee ID
            - first name
            - last name
        - Add to Navbar: Salespeople
    - Add a customer X
        - form
            - first name
            - last name
            - address
            - phone number
        - Add to Navbar: Add a Customer
    - List all customers
        - list (table)
            - first name
            - last name
            - phone number
            - address
        - Add to Navbar: Customers
    - Record a new sale
        - form
            - automobile VIN
            - salesperson
            - customer
            - price
        - NOTE: Special Feature
            - automobiles MUST come from inventory AND be currently unsold
            - already sold automobiles can be detected after a sale
                - look at the sold field from the automobile inventory model
        - Add to Navbar: Add a sale
    - List all sales
        - list (table)
            - Salesperson Employee ID
            - Salesperson Name (1 column w first and last)
            - Customer Name (1 column w first and last)
            - VIN
            - Price
        - Add to Navbar: Sales
    - Salesperson History
        - Dropdown: Choose a salesperson
        - List (table)
            - row for each of specified person's sales
                - Salesperson Name (1 column w first and last)
                - Customer Name (1 column w first and last)
                - VIN
                - Price
        - Add to Navbar: Sales History
